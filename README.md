# Terraform AKS deployment

This is an opinionated deployment of Azure AKS which should:
* Deploys on a precreated vnet/subnet
* Deploys on an independently created AzureAD applicationID
* Deploys with the "advanced" (CNI) network plugin so that pods have IP addresses in the Azure subnet.
* Creates some core namespaces and default-deny network policies inside of them.
* Deploys NGINX ingress controller and Cert-manager for TLS termination of applications (WIP.)
* Deploys Prometheus for monitoring. (WIP)

There is a sample wordpress application per Kubernetes documentation available to test with.
