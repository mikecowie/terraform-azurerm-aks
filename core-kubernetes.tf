provider "kubernetes" {
  host                   = "${azurerm_kubernetes_cluster.aks.kube_config.0.host}"
  client_certificate     = "${base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.client_certificate)}"
  client_key             = "${base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.client_key)}"
  cluster_ca_certificate = "${base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.cluster_ca_certificate)}"
}


#Namespace for non-pod namespaces in operations scope.
resource "kubernetes_namespace" "operations_meta" {
  metadata {
    name = "operations-meta"
  }
}

#

#Operations namespaces from variable
resource "kubernetes_namespace" "namespaces" {
  count = "${length(local.namespaces)}"
  metadata {
    name = "${element(local.namespaces, count.index)}"
  }
}




resource "kubernetes_role" "operations_admin" {
  count = "${length(var.operations_namespaces)}"
  metadata {
    name = "operations-admin"
    namespace = "${element(var.operations_namespaces, count.index)}"
  }
  rule {
    api_groups = ["", "batch", "extensions", "apps"]
    resources = ["*"]
    verbs = ["*"]
  }
}

resource "kubernetes_cluster_role" "operations_admin" {
  metadata {
    name = "operations-admin"
  }
  rule {
    api_groups = [""]
    resources = ["namespaces" ]
    verbs = ["list", "get", "watch"]
  }
}







#Default deny traffic to all operations namespaces (except for kube-system namespace)
#resource "kubernetes_network_policy" "default-deny" {
#  count = "${length(local.namespaces)}"
#  metadata {
#    name = "operations-default-deny"
#    namespace = "${element(local.namespaces, count.index)}"
#  }
#  spec {
#    pod_selector  {}
#    policy_types = ["Ingress", "Egress"]
#  }
#}

#resource "kubernetes_network_policy" "internal-dns" {
#  count = "${length(local.namespaces)}"
#  metadata {
#    name = "kube-dns-allowed"
#    namespace = "${element(local.namespaces, count.index)}"
#  }
#  spec {
#    pod_selector  {}
#    policy_types = ["Ingress", "Egress"]
#    ingress = [
#      {
#        from = [
#          {
#            namespace_selector {
#              match_expressions = [
#                {
#                  key = "name"
#                  operator = "In"
#                  values = ["kube-system"]
#                }
#              ]
#            }
#          },
#          {
#            pod_selector {
#              match_labels {
#                k8s-app = "kube-dns"
#              }
#            }
#          }
#        ]
#      }
#    ]
#      {
#        ports = [
#          {
#            port = "53"
#            protocol = "TCP"
#          },
#          {
#            port = "53"
#            protocol = "UDP"
#          }
#        ]
#
#      }, ]
#  }
#}
