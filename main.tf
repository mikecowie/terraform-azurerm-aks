resource "azurerm_resource_group" "aks" {
  name = "${var.basename}-${var.environment}"
  location = "${var.location}"
  tags = {
    Environment = "${var.environment}"
    Owner = "Mike Cowie"
  }
}

resource "azurerm_virtual_network" "aks" {
  name                = "vnet-${azurerm_resource_group.aks.name}"
  location            = "${azurerm_resource_group.aks.location}"
  resource_group_name = "${azurerm_resource_group.aks.name}"
  address_space       = ["${var.vnet_cidr_range}"]
  tags = {
    environment = "${var.environment}"
    Owner = "Mike Cowie"
  }
}

resource "azurerm_subnet" "aks" {
  name                = "subnet-aks-${azurerm_resource_group.aks.name}"
  resource_group_name = "${azurerm_resource_group.aks.name}"
  virtual_network_name = "${azurerm_virtual_network.aks.name}"
  address_prefix       = "${cidrsubnet(azurerm_virtual_network.aks.address_space[0], 4, 0)}"
}




resource "random_string" "password-aks" {
  length = 16
  special = false
}

resource "azurerm_azuread_application" "aks" {
  name                       = "${azurerm_resource_group.aks.name}"
  homepage                   = "https://${azurerm_resource_group.aks.name}"
  identifier_uris            = ["https://${azurerm_resource_group.aks.name}"]
  reply_urls                 = ["https://${azurerm_resource_group.aks.name}"]
  available_to_other_tenants = false
  oauth2_allow_implicit_flow = true
}

resource "azurerm_azuread_service_principal" "aks" {
  application_id = "${azurerm_azuread_application.aks.application_id}"
}

resource "azurerm_azuread_service_principal_password" "aks" {
  service_principal_id = "${azurerm_azuread_service_principal.aks.id}"
  value                = "${random_string.password-aks.result}"
  end_date             = "2020-01-01T01:02:03Z"
}



resource "azurerm_kubernetes_cluster" "aks" {
  name                = "aks-${azurerm_resource_group.aks.name}"
  location            = "${azurerm_resource_group.aks.location}"
  resource_group_name = "${azurerm_resource_group.aks.name}"
  dns_prefix          = "${var.aks_dns_prefix}"
  kubernetes_version  = "1.12.7"
  role_based_access_control {
    enabled = true
  }
  agent_pool_profile {
    name            = "default"
    count           = 1
    vm_size         = "Standard_D1_v2"
    os_type         = "Linux"
    os_disk_size_gb = 30
    vnet_subnet_id = "${azurerm_subnet.aks.id}"
  }
  service_principal {
    client_id     = "${azurerm_azuread_service_principal.aks.application_id}"
    client_secret = "${azurerm_azuread_service_principal_password.aks.value}"
  }
  network_profile {
    network_plugin = "azure"
#    network_policy = "calico" #Haven't yet made this work
#    dns_service_ip = ""
#    docker_bridge_cidr = ""
#    service_cidr = ""
  }
  tags = {
    environment = "Production"
    Owner = "Mike Cowie"
  }
}

resource "local_file" "kubeconfig" {
  content = "${azurerm_kubernetes_cluster.aks.kube_config_raw}"
  filename = "/home/mikec/.kube/config"
}


resource "azurerm_role_assignment" "aks-subnet" {
  role_definition_name = "Contributor"
  scope = "${azurerm_resource_group.aks.id}"
  principal_id = "${azurerm_azuread_service_principal.aks.id}"
}




resource "azurerm_container_registry" "aks" {
  name                = "aksregistryccltest1"
  count = 1
  location            = "${azurerm_resource_group.aks.location}"
  resource_group_name = "${azurerm_resource_group.aks.name}"
  sku                 = "Basic"
}
