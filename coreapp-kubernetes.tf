resource "template_dir" "ingress-external" {
  depends_on = ["azurerm_kubernetes_cluster.aks"]
  source_dir = "${path.module}/ingress"
  destination_dir = "${path.module}/tmp/ingress-external"
  vars {
    namespace = "operations-ingress-external"
    internal = "false"
  }
}

#resource "null_resource" "ingress-external" {
#  depends_on = ["template_dir.ingress-external"]
#  triggers {
#    main = "${sha1(file("${path.module}/ingress/main.yml"))}"
#    service = "${sha1(file("${path.module}/ingress/service.yml"))}"
#  }
#  provisioner "local-exec" {
#    command = "kubectl apply -f ${path.module}/tmp/ingress-external"
#  }
#  provisioner "local-exec" {
#    command = "kubectl delete -f ${path.module}/tmp/ingress-external"
#    when = "destroy"
#  }
#}

resource "template_dir" "ingress-internal" {
  depends_on = ["azurerm_kubernetes_cluster.aks"]
  source_dir = "${path.module}/ingress"
  destination_dir = "${path.module}/tmp/ingress-internal"
  vars {
    namespace = "operations-ingress-internal"
    internal = "true"
  }

}

#resource "null_resource" "ingress-internal" {
#  depends_on = ["template_dir.ingress-internal"]
#  triggers {
#     main = "${sha1(file("${path.module}/ingress/main.yml"))}"
#     service = "${sha1(file("${path.module}/ingress/service.yml"))}"
#  }
#  provisioner "local-exec" {
#    command = "kubectl apply -f ${path.module}/tmp/ingress-internal"
#  }
#  provisioner "local-exec" {
#    command = "kubectl delete -f ${path.module}/tmp/ingress-internal"
#    when = "destroy"
#  }
#}
