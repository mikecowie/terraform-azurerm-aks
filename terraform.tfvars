location = "australiaeast"
basename = "aks"
environment = "test"
vnet_cidr_range = "10.140.0.0/16"
aks_dns_prefix = "ccltest"
operations_namespaces = ["operations-ingress", "operations-monitoring"]
application_namespaces = ["application-wordpress-prod", "application-cacti-prod"]
