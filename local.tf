locals {
  namespaces = "${concat(var.operations_namespaces, var.application_namespaces)}"
}
