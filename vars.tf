variable "basename" {}
variable "location" {}
variable "environment" {}
variable "vnet_cidr_range" {}
variable "aks_dns_prefix" {}

variable "operations_namespaces" {
  type = "list"
}

variable "application_namespaces" {
  type = "list"
}
